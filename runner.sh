#!/bin/bash
which node
if [[ "${ENVIRONMENT}" == "development" ]]; then
	nodemon index.js | tee logs/stdout.log
else
	node index.js
fi