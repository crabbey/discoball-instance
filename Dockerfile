FROM node:10-stretch

# Create app directory
WORKDIR /usr/src/app

# Install bash, python, make, and gcc for node-gyp.
RUN apt install bash && \
    apt install g++ make python git

# Install node-gyp and nodemon
RUN npm install --global nodemon node-gyp

# Create group and user
RUN addgroup --gid 500 --system discoball
RUN adduser --system --home /usr/src/app --shell /sbin/nologin --gid 500 --uid 500 discoball

COPY package.json package.json
RUN npm install

COPY . .

RUN chmod +x runner.sh

VOLUME ["/usr/src/app/config", "/usr/src/app/modules"]

USER discoball

ENTRYPOINT ["./runner.sh"]