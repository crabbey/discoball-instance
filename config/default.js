const path = require('path');


// Ensure we always know the project root.
// If the config file is not loaded from ./config/, update this.
const baseDir = path.dirname(path.dirname(path.resolve(__filename)));

module.exports = {
  discoball: {
    baseDir: baseDir,

    // Security warning: Do not run in production with debug turned on!
    debug: true,
  }
};
